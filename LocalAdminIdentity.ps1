Import-Module activedirectory
# cd "\\sw03501608014\avt\Work\Скрипты\LocalAdminIdentity\"
$ListFile = ".\LocalAdminList"
Clear-Content $ListFile
$WorkStations = Get-Content ".\ADWSList.csv"

foreach ($ws in $WorkStations){
                
        if (-not (Test-Connection $ws -Count 1 2>$null)){
            write-host "$ws not connected"
            continue
        }

        $LocalGroup =[ADSI]"WinNT://$ws/Администраторы"
        $UserName = @($LocalGroup.psbase.Invoke("Members"))
        $UserName = $UserName | foreach {$_.GetType().InvokeMember("Name", 'GetProperty', $null, $_, $null)} | Where-Object {$_ -match '^035.{1,}'}
     
        If ($UserName -gt "") {
            foreach ($u in $UserName){
                [string]$ws + ' ' + $u | Out-File $ListFile -encoding Unicode -append
            }
        } 
}

# ([ADSI]"WinNT://$ws/Администраторы,group").remove("WinNT://0035PFRRU/$UserLogin")