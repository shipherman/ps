﻿<#
source  https://gist.github.com/Mine02C4/6242585


Атрибут: 195 (С3) Hardware ECC Recovered
    Тип 	накапливающий
    Описание 	содержит количество ошибок, которые были скорректированы аппаратными средствами ECC диска
Особенности, присущие этому атрибуту на разных дисках, полностью соответствуют таковым атрибутов 01 и 07.



Атрибут: 196 (С4) Reallocated Event Count
    Тип 	накапливающий
    Описание 	содержит количество операций переназначения секторов
Косвенно говорит о здоровье диска. Чем больше значение — тем хуже. Однако нельзя однозначно судить о здоровье диска по этому параметру, не рассматривая другие атрибуты


Атрибут: 197 (С5) Current Pending Sector Count
    Тип 	текущий
    Описание 	содержит количество секторов-кандидатов на переназначение в резервную область
Натыкаясь в процессе работы на «нехороший» сектор (например, контрольная сумма сектора не соответствует данным в нём), диск помечает его как кандидат на переназначение, заносит его в специальный внутренний список и увеличивает параметр 197. Из этого следует, что на диске могут быть повреждённые секторы, о которых он ещё не знает — ведь на пластинах вполне могут быть области, которые винчестер какое-то время не использует.


Атрибут: 198 (С6) Offline Uncorrectable Sector Count (Uncorrectable Sector Count)
    Тип 	текущий
    Описание 	означает то же самое, что и атрибут 197, но отличие в том, что данный атрибут содержит количество секторов-кандидатов, обнаруженных при одном из видов самотестирования диска — оффлайн-тестировании, которое диск запускает в простое в соответствии с параметрами, заданными прошивкой
Параметр этот изменяется только под воздействием оффлайн-тестирования, никакие сканирования программами на него не влияют. При операциях во время самотестирования поведение атрибута такое же, как и атрибута 197.
Ненулевое значение говорит о неполадках на диске (точно так же, как и 197, не конкретизируя, кто виноват).
#>



# $output_file = '\\sw03501608014\gp\PSHDD_S.M.A.R.T\hdd_smart_info.html'
$computers = get-content '\\sw03501608014\gp\PSHDD_S.M.A.R.T\adwslist.csv' #@('ws03501608004', 'ws03501608001', 'ws03501608003') 
$parameters = @{#C2 = 'Temperature (HDA Temperature, HDD Temperature)'
                #C3 = 'Hardware ECC Recovered'
                C4 = 'Reallocated Event Count'
                C5 = 'Current Pending Sector Count'
                C6 = 'Offline Uncorrectable Sector Count (Uncorrectable Sector Count)'
                }


function Get-SmartData{
    param (
        [string]$computername="$env:COMPUTERNAME"
    )
    $smart = gwmi -computername $computername -Namespace root\WMI -Class `
        MSStorageDriver_FailurePredictData -ErrorAction:SilentlyContinue
    if ($smart.VendorSpecific.Length -gt 0) {
        $smart = @($smart)
    }
    foreach ($disk in $smart) {
        $result = @()
        for ($i = 2; $i -lt $disk.VendorSpecific.Length; $i += 12) {
            $AttrID = [System.BitConverter]::ToString($disk.VendorSpecific[$i])
            if ($AttrID -in $parameters.keys) {
                $result += [pscustomobject] @{
                    ComputerName = $computername;
                    AttrID = $AttrID;
                    #StatusFlag1 = $disk.VendorSpecific[$i+1];
                    #StatusFlag2 = $disk.VendorSpecific[$i+2];
                    #AttrValue = $disk.VendorSpecific[$i+3];
                    #WorstValue = $disk.VendorSpecific[$i+4];
                    UserFriendlyName = $parameters.$AttrID
                    Raws = $disk.VendorSpecific[($i+5)..($i+10)];
                    #Reserved = $disk.VendorSpecific[$i+11];
                }
                #write-host $result.Raws.gettype().fullname
            }
        }
        foreach ($value in $result.raws){
            if ($value -ne 0){
                $result
                break
            }
        }
    }
}


function Send-eMail {
    param(
        $email_body = [string](Get-content $output_file)
    )
if ($email_body){
        $encoding = [system.text.encoding]::UTF7
        Send-MailMessage -SmtpServer '10.35.0.11' `
            -From '160801 <160801@035.pfr.ru>' `
            -To 'admin <160801@035.pfr.ru>' `
            -Subject 'HDD S.M.A.R.T. Alert' -Body $email_body `
            -BodyAsHtml -Encoding $encoding
    }
}


foreach ($comp in $computers){
    $info += Get-SmartData -computername $comp
}

#Get-SmartData -computername ws03501608004

if ($info){
    [string]$email_body = $info | %{[pscustomobject]@{ComputerName = $_.computername;
                                AttrID = $_.AttrID;
                                UserFriendlyName = $_.UserFriendlyName; 
                                Raws = [string]$_.Raws}} | convertto-html
                                #| Out-File $output_file
}

Send-eMail -email_body $email_body