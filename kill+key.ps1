<# I forget which versions this applies to, but some of the older versions of PowerShell launch the console in MTA mode by default, while the ISE is always STA. On my computer with PowerShell 4.0 installed, this is no longer the case; both ISE and Console both use STA by default.

Anyhow, just use the -Sta switch when launching PowerShell.exe, and you'll be able to run that script. You could even add code to the beginning of the script which detects the current apartment state, and relaunches a new copy of powershell.exe if needed, very similar to code which people have used to detect whether PowerShell.exe was running in an elevated process. Something along these lines:

	if ([System.Threading.Thread]::CurrentThread.ApartmentState -eq [System.Threading.ApartmentState]::MTA)
	{
		powershell.exe -Sta -File $MyInvocation.MyCommand.Path
		return
	}
	# Rest of your script code here

src https://social.technet.microsoft.com/Forums/lync/en-US/4d19737b-8fa8-4696-b4f8-d1afa03f2b47/singol-thread-error-sta-when-open-script-by-powershell?forum=ITCG
#>

if ([System.Threading.Thread]::CurrentThread.ApartmentState -eq [System.Threading.ApartmentState]::MTA)
{
    powershell.exe -Sta -File $MyInvocation.MyCommand.Path
    return
}

[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")

Stop-Process -Name Monitor, wmail, CLite -Force

$dstPath = '\\sw03501608014\p\DST\'
$dst = ''
$crypto32path = "C:\Program Files\InfoTeCS\ViPNet CryptoService"
$vipnet32path = "C:\Program Files\InfoTeCS\ViPNet Client"
$vipnet64path = "C:\Program Files (x86)\InfoTeCS\ViPNet Client"
$path = ''
$csp = ''

if (Test-Path $crypto32path) {
    $path = $crypto32path
    }
elseif (Test-Path $vipnet32path) {
    $path = $vipnet32path
    }
elseif (Test-Path $vipnet64path) {
    $path = $vipnet64path
    }

$csp = (Split-Path $path)+'\ViPNet CSP\'
# ���� ����� ��
$abn_num = Get-ChildItem $path -Filter "user_*" -Name
$abn_num = $abn_num -replace "user_"
$dst = (Get-ChildItem $dstPath -Recurse -Include "abn_$abn_num.dst" -Name)


if ($dst -gt ''){
	&$path\KeySetup.exe /clean /td $path
    # ������� ��������� ��� ����, ����� ������ ����������� �������, ����� ������� ������
	timeout 5
    $dstFullPath = $dstPath + $dst
    &$path\KeySetup.exe $dstFullPath 
    $ans = read-host -Prompt '������� dst?(y/n)'
    if ($ans -eq 'y' -or $ans -eq '') {
         $dstFullPath = Split-Path $dstFullPath | Split-Path
         Remove-Item $dstFullPath -Recurse -Force
        }
    }
else {
    echo 'dst ���� �� ������'
    }

    
try {
    &$path\monitor.exe
    }
catch [System.Management.Automation.CommandNotFoundException] {
    write-host '������ ������� �� ���������'
    }
try {
    &$path\wmail.exe
    }
catch [System.Management.Automation.CommandNotFoundException] {
    write-host '������� ����� �� ����������'
    }
try {
    &$path\CLite.exe
    }
catch [System.Management.Automation.CommandNotFoundException] {
    write-host '������������ �� ���������'
    }
finally {
    &$csp\csp_settings_app.exe
    }
# ���� C:\Program Files\InfoTeCS\ViPNet Client\user_xxxx\key_disk\dom\ ���������� � ����� ������, ����� � CSP �� �������� ��� �������
[System.Windows.Forms.Clipboard]::SetText($path + '\user_' + $abn_num + '\key_disk\dom\')

read-host '������ ��� ����������...'