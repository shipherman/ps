﻿[void][reflection.assembly]::LoadWithPartialName("System.Windows.Forms")

$form = new-object Windows.Forms.Form
$form.Font = New-Object System.Drawing.Font("Verdana",14)
$Form.AutoScroll = $True
$form.AutoSize = $True
$Form.AutoSizeMode = "GrowAndShrink"

$butA = new-object Windows.Forms.Button
$butA.text="Сканировать"
$butA.add_click({&"P:\NAPS2\NAPS2.Console.exe" -i 'd:\Сканированное\temp.pdf' -o 'd:\Сканированное\temp.pdf' -f -p Panasonic})
$butA.AutoSize = $True

$butB = new-object Windows.Forms.Button
$butB.Font = New-Object System.Drawing.Font("Verdana",10)
$butB.text="Следующий документ"
$butB.add_click({$ts=get-date -uformat "%Y%m%H%M%S"; copy-item d:\Сканированное\temp.pdf d:\Сканированное\$ts.pdf; Remove-Item d:\Сканированное\temp.pdf})
$butB.AutoSize = $True
$butB.Location = New-Object System.Drawing.Point(0,40)


$form.Text = "Panasonic"
$form.controls.add($butA)
$form.controls.add($butB)
$form.Add_Shown({$form.Activate()})
$form.ShowIcon = 0
$form.ShowDialog()
$Form.FormBorderStyle = "FixedSingle"
$Form.StartPosition = "Manual"